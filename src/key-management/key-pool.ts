import type * as http from "http";
import { AnthropicKeyProvider, AnthropicKeyUpdate } from "./anthropic/provider";
import { GoogleKeyProvider, GoogleKeyUpdate } from "./google/provider";
import { Ai21KeyProvider, Ai21KeyUpdate } from "./ai21/provider";
import { Key, Model, KeyProvider, AIService } from "./index";
import { OpenAIKeyProvider, OpenAIKeyUpdate } from "./openai/provider";
import { GroqKeyProvider, GroqKeyUpdate } from "./groq/provider";
import { MistralKeyProvider, MistralKeyUpdate } from "./mistral/provider";



type AllowedPartial = OpenAIKeyUpdate | AnthropicKeyUpdate | GoogleKeyUpdate | Ai21KeyUpdate | GroqKeyUpdate | MistralKeyUpdate;

export class KeyPool {
  private keyProviders: KeyProvider[] = [];

  constructor() {
    this.keyProviders.push(new OpenAIKeyProvider());
    this.keyProviders.push(new AnthropicKeyProvider());
	this.keyProviders.push(new GoogleKeyProvider());
	this.keyProviders.push(new Ai21KeyProvider());
	this.keyProviders.push(new GroqKeyProvider());
	this.keyProviders.push(new MistralKeyProvider());
  }
  
  
  public init() {
    this.keyProviders.forEach((provider) => provider.init());
    const availableKeys = this.available("all");
    if (availableKeys === 0) {
      throw new Error(
        "No keys loaded. Ensure either OPENAI_KEY or ANTHROPIC_KEY or GOOGLE_KEY or AI21_KEY or GROQ_KEY or MISTRAL_KEY is set."
      );
    }
  }
  
  public getKeysSafely() {
	const openaiKeys = this.keyProviders[0].getAllKeys();
	const anthropipcKeys = this.keyProviders[1].getAllKeys();
	const googleKeys = this.keyProviders[2].getAllKeys();
	const ai21Keys = this.keyProviders[3].getAllKeys();
	const groqKeys = this.keyProviders[4].getAllKeys();
    const mistralKeys = this.keyProviders[5].getAllKeys();

	const combinedKeys = Array.prototype.concat.call(openaiKeys, anthropipcKeys, googleKeys, ai21Keys, groqKeys, mistralKeys);
	return combinedKeys;
  }
  
  public addKey(key: string) {
	  const openaiProvider = this.keyProviders[0]
	  const anthropicProvider = this.keyProviders[1]
	  const googleProvider = this.keyProviders[2]
	  const ai21Provider = this.keyProviders[3]
	  const groqProvider = this.keyProviders[4];
	  const mistralProvider = this.keyProviders[5]
	  
	  let val = false
	  if (key.includes("sk-ant-api") || key.startsWith("AKIA")) {
		val = anthropicProvider.addKey(key);
	  } else if (key.includes("sk-") || key.includes(".azure.")) {
		val = openaiProvider.addKey(key);
	  } else if (key.includes("AIzaSy")) {
		val = googleProvider.addKey(key);
	  } else if (key.includes("gsk_")) {
		val = groqProvider.addKey(key);
	  } else if (key.startsWith("MAKIA")) {
		val = mistralProvider.addKey(key.slice(1));
	  } else {
		  val = mistralProvider.addKey(key);
	  }

	  return val;
	  
  }
  public getKeyByHash(keyHash: string) {
	const openaiProvider = this.keyProviders[0]
	const anthropicProvider = this.keyProviders[1]
	const googleProvider = this.keyProviders[2]
	const ai21Provider = this.keyProviders[3]
	const groqProvider = this.keyProviders[4]
	const mistralProvider = this.keyProviders[5]
	
	const prefix = keyHash.substring(0, 3);

	if (prefix === 'oai') {
		let key = openaiProvider.getKeyByHash(keyHash);
		return key  
	} else if (prefix === 'ant') { 
    	let key = anthropicProvider.getKeyByHash(keyHash);
		return key 
	} else if (prefix === 'pal') { 
    	let key = googleProvider.getKeyByHash(keyHash);
		return key 
	} else if (prefix === 'ai2') { 
    	let key = ai21Provider.getKeyByHash(keyHash);
		return key
	} else if (prefix === 'gro') { 
    	let key = groqProvider.getKeyByHash(keyHash);
		return key
	} else if (prefix === 'mist') { 
    	let key = mistralProvider.getKeyByHash(keyHash);
		return key
	} else {
		return false
	}
	
  
  }
  
  public deleteKeyByHash(keyHash: string) {
	const openaiProvider = this.keyProviders[0]
	const anthropicProvider = this.keyProviders[1]
	const googleProvider = this.keyProviders[2]
	const ai21Provider = this.keyProviders[3]
	const groqProvider = this.keyProviders[4]
	const mistralProvider = this.keyProviders[5]

	const prefix = keyHash.substring(0, 3);
	if (prefix === 'oai') {
		openaiProvider.deleteKeyByHash(keyHash);
		return true 
	} else if (prefix === 'ant') { 
    	anthropicProvider.deleteKeyByHash(keyHash);
		return true 
	} else if (prefix === 'pal') { 
    	googleProvider.deleteKeyByHash(keyHash);
		return true 
	} else if (prefix === 'ai2') { 
    	ai21Provider.deleteKeyByHash(keyHash);
		return true 	
	} else if (prefix === 'gro') { 
    	groqProvider.deleteKeyByHash(keyHash);
		return true 	
	} else if (prefix === 'mis') { 
    	mistralProvider.deleteKeyByHash(keyHash);
		return true 	
	} else {
		// Nothing invalid key, shouldn't be possible (Maybe in future handle error)
		return false
	}
  }
  
  
  public getHashes() {
	const combinedHashes: string[] = [];
	this.keyProviders.forEach((provider) => {
		const hashes = provider.getHashes();
		combinedHashes.push(...hashes);
	})
	
	return combinedHashes;
  }
  
  
  public recheck() {
	this.keyProviders.forEach((provider) => {
		provider.recheck();
	})
	const availableKeys = this.available("all");
  }

  public get(model: Model, applyRateLimit: boolean = true): Key {
    const service = this.getService(model);
	return this.getKeyProvider(service).get(model, applyRateLimit);
  }
  

  public list(): Omit<Key, "key">[] {
    return this.keyProviders.flatMap((provider) => provider.list());
  }

  public disable(key: Key, reason: "quota" | "revoked"): void {
    const service = this.getKeyProvider(key.service);
    service.disable(key);
    if (service instanceof OpenAIKeyProvider || service instanceof GroqKeyProvider || service instanceof MistralKeyProvider) {
      service.update(key.hash, {
        isRevoked: reason === "revoked",
        isOverQuota: reason === "quota",
      });
    }
  }

  public update(key: Key, props: AllowedPartial): void {
    const service = this.getKeyProvider(key.service);
    service.update(key.hash, props);
  }

  public available(service: AIService | "all" = "all"): number {
    return this.keyProviders.reduce((sum, provider) => {
      const includeProvider = service === "all" || service === provider.service;
      return sum + (includeProvider ? provider.available() : 0);
    }, 0);
  }

  public anyUnchecked(): boolean {
    return this.keyProviders.some((provider) => provider.anyUnchecked());
  }

  public incrementPrompt(key: Key): void {
    const provider = this.getKeyProvider(key.service);
    provider.incrementPrompt(key.hash);
  }

  public getLockoutPeriod(model: Model): number {
    const service = this.getService(model);
    return this.getKeyProvider(service).getLockoutPeriod(model);
  }

  public markRateLimited(key: Key): void {
    const provider = this.getKeyProvider(key.service);
    provider.markRateLimited(key.hash);
  }

  public updateRateLimits(key: Key, headers: http.IncomingHttpHeaders): void {
    const provider = this.getKeyProvider(key.service);
    if (provider instanceof OpenAIKeyProvider || provider instanceof GroqKeyProvider) {
      provider.updateRateLimits(key.hash, headers);
    }
  }



  private getService(model: Model): AIService {
	if (model.includes("bison") || model.includes("gemini")) {
	  return "google";
	} else if (model.startsWith("j2-")) {
      return "ai21";
    } else if (model.startsWith("gpt") || model.startsWith("dall-") || model.startsWith("text-embedding-") || model.startsWith("chatgpt") || model.startsWith("o1-")) {
      return "openai";
    } else if (model.startsWith("claude-") || model.startsWith("anthropic.")) {
      return "anthropic";
    } else if (model.startsWith("mistral") || model.startsWith("mixtral") || model.startsWith("open-")) {
      return "mistral";
    } else if (model.startsWith("llama") || model.startsWith("mixtral") || model.startsWith("gemma")) {
      return "groq";
    }
	
	
    throw new Error(`Unknown service for model '${model}'`);
  }

  private getKeyProvider(service: AIService): KeyProvider {
    return this.keyProviders.find((provider) => provider.service === service)!;
  }
}
