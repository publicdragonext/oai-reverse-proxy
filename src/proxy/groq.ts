import { Request, RequestHandler, Router } from "express";
import * as http from "http";
import { createProxyMiddleware } from "http-proxy-middleware";
import { config } from "../config";
import { logger } from "../logger";
import { createQueueMiddleware } from "./queue";
import { ipLimiter } from "./rate-limit";
import { handleProxyError } from "./middleware/common";
import {
  addKey,
  blockZoomerOrigins,
  createPreprocessorMiddleware,
  finalizeBody,
  languageFilter,
  removeOriginHeaders,
} from "./middleware/request";
import {
  ProxyResHandlerWithBody,
  createOnProxyResHandler,
} from "./middleware/response";

let modelsCache: any = null;
let modelsCacheTime = 0;

const getModelsResponse = () => {
  if (new Date().getTime() - modelsCacheTime < 1000 * 60) {
    return modelsCache;
  }

  if (!config.groqKey) return { object: "list", data: [] };

  const groqVariants = [
    "llama-3.1-405b-reasoning",
	"llama-3.1-70b-versatile",
	"llama-3.1-8b-instant",
	"llama3-70b-8192",
	"llama3-8b-8192",
	"mixtral-8x7b-32768",
	"gemma-7b-it",
	"gemma2-9b-it"
  ]; 

  const models = groqVariants.map((id) => ({
    // MAY NEED CHANGE 
    id,
    object: "model",
    created: new Date().getTime(),
    owned_by: "groq",
    permission: [],
    root: "openai",
    parent: null,
  }));

  modelsCache = { object: "list", data: models };
  modelsCacheTime = new Date().getTime();

  return modelsCache;
};

const handleModelRequest: RequestHandler = (_req, res) => {
  res.status(200).json(getModelsResponse());
};


const removeStreamProperty = (
  proxyReq: http.ClientRequest,
  req: Request,
  res: http.ServerResponse,
  options: any
) => {
  if (req.body && typeof req.body === "object") {
    // delete req.body.stream;
  }
};

const rewriteGroqRequest = (
  proxyReq: http.ClientRequest,
  req: Request,
  res: http.ServerResponse
) => {

  delete req.body.logprobs
  delete req.body.logit_bias
  delete req.body.top_logprobs
  

  const rewriterPipeline = [
    addKey,
    languageFilter,
    blockZoomerOrigins,
    removeOriginHeaders,
    // removeStreamProperty,
	finalizeBody,
  ];

  try {
	
    for (const rewriter of rewriterPipeline) {
	  
      rewriter(proxyReq, req, res, {});
    }
  } catch (error) {
    req.log.error(error, "Error while executing proxy rewriter");
    proxyReq.destroy(error as Error);
  }
  
  
};

/** Only used for non-streaming requests. */
const groqResponseHandler: ProxyResHandlerWithBody = async (
  _proxyRes,
  req,
  res,
  body
) => {
	
  if (typeof body !== "object") {
    throw new Error("Expected body to be an object");
  }


 

  res.status(200).json(body);
};




const groqProxy = createQueueMiddleware({
  proxyMiddleware: createProxyMiddleware({
    target: "https://api.groq.com/openai/v1/chat/completions",
    changeOrigin: true,
    on: {
      proxyReq: rewriteGroqRequest,
      proxyRes: createOnProxyResHandler([groqResponseHandler]),
      error: handleProxyError,
    },
    selfHandleResponse: true,
    logger,
	  pathRewrite: {
	  '^/proxy/groq/chat/completions': '', 
	  '^/v1/chat/completions': '', 
	  '^/proxy': ''
	  }
  })
});

const groqRouter = Router();

// Fix paths because clients don't consistently use the /v1 prefix.
groqRouter.use((req, _res, next) => {
  if (!req.path.startsWith("/v1/")) {
    req.url = `/v1${req.url}`;
  }
  next();
});
groqRouter.get("/v1/models", handleModelRequest);
groqRouter.post(
  "/v1/complete",
  ipLimiter,
  createPreprocessorMiddleware({ inApi: "groq", outApi: "groq" }),
  groqProxy
);

// OpenAI-to-Groq compatibility endpoint. / Basically do nothing 
groqRouter.post(
  "/v1/chat/completions",
  ipLimiter,
  createPreprocessorMiddleware({ inApi: "openai", outApi: "groq" }),
  (req, res, next) => {
    groqProxy(req, res, next);
  }
);
// Redirect browser requests to the homepage.
groqRouter.get("*", (req, res, next) => {
  const isBrowser = req.headers["user-agent"]?.includes("Mozilla");
  if (isBrowser) {
    res.redirect("/");
  } else {
    next();
  }
});

export const groq = groqRouter;
