/* Accepts incoming requests at either the /kobold or /openai routes and then
routes them to the appropriate handler to be forwarded to the OpenAI API.
Incoming OpenAI requests are more or less 1:1 with the OpenAI API, but only a
subset of the API is supported. Kobold requests must be transformed into
equivalent OpenAI requests. */

import * as express from "express";
import { gatekeeper } from "./auth/gatekeeper";
import { checkRisuToken } from "./auth/check-risu-token";
import { kobold } from "./kobold";
import { openai } from "./openai";
import { anthropic } from "./anthropic";
import { google } from "./google";
import { ai21 } from "./ai21";
import { groq } from "./groq";
import { mistral } from "./mistral";
import { config } from "../config"; 

// need to move a lot of thigns 
const ai21Variants = [
    "j2-ultra"
  ]; 
  
const mistralVariants = [
// Excluded any deprecated production models.
// 32k 
"codestral-latest",
"codestral-2405",

"open-mistral-7b",
"open-mixtral-8x7b",

// 64k 
"open-mixtral-8x22b",

// 128k
"mistral-large-latest",
"mistral-large-2407",

"open-mistral-nemo",
"open-mistral-nemo-2407",

// 256k
"open-codestral-mamba",
]; 

const claudeVariants = [
    "claude-v1",
    "claude-v1-100k",
    "claude-instant-v1",
    "claude-instant-v1-100k",
    "claude-v1.3",
    "claude-v1.3-100k",
    "claude-v1.2",
    "claude-v1.0",
    "claude-instant-v1.1",
    "claude-instant-v1.1-100k",
    "claude-instant-v1.0",
    "claude-2",
    "claude-2.0",
    "claude-2.1",
	"claude-3-opus-20240229",
	"claude-3-sonnet-20240229",
	"anthropic.claude-v1",
	"anthropic.claude-v2",
	"anthropic.claude-v2:1",
	"anthropic.claude-instant-v1",
	"anthropic.claude-3-sonnet-20240229-v1:0",
  "anthropic.claude-3-haiku-20240307-v1:0",
  "anthropic.claude-3-opus-20240229-v1:0",
  "claude-3-haiku-20240307",
  "anthropic.claude-3-5-sonnet-20240620-v1:0",
  "claude-3-5-sonnet-20240620"
];

const gptVariants = [
    "dall-e-2",
	"dall-e-3",
	
	"o1-mini",
	"o1-preview",
	
	"chatgpt-4o-latest", // kek 
    "gpt-4o-mini",
    "gpt-4o-mini-2024-07-18",
    "gpt-4o",
    "gpt-4o-2024-05-13",
	"gpt-4o-2024-08-06",
    "gpt-4",
    "gpt-4-0613",
    "gpt-4-0314",
    "gpt-4-32k",
    "gpt-4-32k-0613",
    "gpt-4-32k-0314",
    "gpt-4-1106-preview",
    "gpt-4-0125-preview",
    "gpt-4-turbo",
    "gpt-4-turbo-2024-04-09",
    "gpt-4-turbo-preview",
    "gpt-4-vision-preview",
    "gpt-4-1106-vision-preview",
    "gpt-3.5-turbo-1106", 
    "gpt-3.5-turbo",
    "gpt-3.5-turbo-0301",
    "gpt-3.5-turbo-0613",
    "gpt-3.5-turbo-16k",
    "gpt-3.5-turbo-16k-0613",
	"gpt-3.5-turbo-instruct",
    "gpt-3.5-turbo-instruct-0914",
	
	// Moderated 
	"gpt-3.5-turbo-moderated",
	"gpt-3.5-turbo-16k-moderated",
	"gpt-4o-moderated" ,
	"gpt-4-turbo-moderated" ,
	"gpt-4-32k-moderated" ,
	
	// Embedings support:
	"text-embedding-ada-002",
	"text-embedding-3-small",
	"text-embedding-3-large"
  ];
  
const googleVariants = [
  "chat-bison-001",
  "text-bison-001",
  "embedding-gecko-001",
  "gemini-1.0-pro-latest",
  "gemini-1.0-pro",
  "gemini-pro",
  "gemini-1.0-pro-001",
  "gemini-1.0-pro-vision-latest",
  "gemini-pro-vision",
  "gemini-1.0-ultra-latest",
  "gemini-ultra",
  "gemini-1.5-pro-latest",
  "gemini-1.5-pro-001",
  "gemini-1.5-pro",
  "gemini-1.5-flash-latest",
  "gemini-1.5-flash-001",
  "gemini-1.5-flash",
  "gemini-1.5-pro-exp-0801",
  "gemini-1.5-pro-exp-0827",
  "gemini-1.5-flash-exp-0827",
  "gemini-1.5-flash-8b-exp-0827"
]

const groqVariants = [
    "llama-3.1-405b-reasoning",
	"llama-3.1-70b-versatile",
	"llama-3.1-8b-instant",
	"llama3-70b-8192",
	"llama3-8b-8192",
	"mixtral-8x7b-32768",
	"gemma-7b-it",
	"gemma2-9b-it"
];


const proxyRouter = express.Router();
const streamRouter = express.Router();

proxyRouter.use(
  express.json({ limit: "100mb" }),
  express.urlencoded({ extended: true, limit: "100mb" })
);
proxyRouter.use(gatekeeper);
proxyRouter.use(checkRisuToken);

proxyRouter.use((req, _res, next) => {
  req.startTime = Date.now();
  req.retryCount = 0;
  next();
});

proxyRouter.use("/kobold", kobold);
proxyRouter.use("/openai", openai);
proxyRouter.use("/anthropic", anthropic);
proxyRouter.use("/google-ai", google);
proxyRouter.use("/ai21", ai21);
proxyRouter.use("/groq", groq);
proxyRouter.use("/mistral", mistral);





// Need to move it all for now proof of concept for universal endpoint:




proxyRouter.get("(\/v1)?\/models", (req, res, next) => {
  const modelsResponse = getModelsResponse();
  res.json(modelsResponse);
});

proxyRouter.get("(\/stream)(\/v1)?\/models", (req, res, next) => {
  const modelsResponse = getModelsResponse();
  res.json(modelsResponse);
});


const modelVariantHandlers = {
  ...Object.fromEntries(mistralVariants.map(variant => [variant, mistral])),
  ...Object.fromEntries(groqVariants.map(variant => [variant, groq])),
  ...Object.fromEntries(ai21Variants.map(variant => [variant, ai21])),
  ...Object.fromEntries(claudeVariants.map(variant => [variant, anthropic])),
  ...Object.fromEntries(gptVariants.map(variant => [variant, openai])),
  ...Object.fromEntries(googleVariants.map(variant => [variant, google])),
};


// Required as 
proxyRouter.post(/^(\/v1|\/v1beta)?\/((chat\/completions|complete|messages|embeddings|images\/generations)|models\/([^\/]+)(\:generateContent|\:streamGenerateContent))?(\?key=([^"&\s]+))?$/, (req, res, next) => {
  const { model } = req.body;

  if (!model) {
    const pathModel = req.originalUrl.split('/')[3].split(":")[0];
	req.body.model = pathModel
	const handler = modelVariantHandlers[pathModel];

	if (handler) {
		handler(req, res, next);
	} else {
		// Handle invalid model variant
		res.status(400).json({ error: "Invalid model" });
	}	
  } else {  
	const handler = modelVariantHandlers[model];
	if (handler) {
		handler(req, res, next);
	} else {
		// Handle invalid model variant
		res.status(400).json({ error: "Invalid model" });
	}
  }
});


streamRouter.post(/^(\/v1|\/v1beta)?\/((chat\/completions|complete|messages|embeddings|images\/generations)|models\/([^\/]+)(\:generateContent|\:streamGenerateContent))?(\?key=([^"&\s]+))?$/, (req, res, next) => {
  req.body.stream = true;
  const { model } = req.body;
  
  if (!model) {
    const model = req.originalUrl.split('/')[3];
	const handler = modelVariantHandlers[model];
	if (handler) {
		handler(req, res, next);
	} else {
		// Handle invalid model variant
		res.status(400).json({ error: "Invalid model" });
	}	
  } else {  
	const handler = modelVariantHandlers[model];
	if (handler) {
		handler(req, res, next);
	} else {
		// Handle invalid model variant
		res.status(400).json({ error: "Invalid model" });
	}
  }
});


proxyRouter.use('/stream', streamRouter);
let modelsCache: any = null;
let modelsCacheTime = 0;

const allModels: string[] = [];
if (config.ai21Key) {
  allModels.push(...ai21Variants);
}
if (config.anthropicKey) {
  allModels.push(...claudeVariants);
}
if (config.openaiKey) {
  allModels.push(...gptVariants);
}
if (config.googleKey) {
  allModels.push(...googleVariants);
}

if (config.groqKey) {
  allModels.push(...groqVariants);
}

if (config.mistralKey) {
  allModels.push(...groqVariants);
}
  
  
const getModelsResponse = () => {
  if (new Date().getTime() - modelsCacheTime < 1000 * 60) {
    return modelsCache;
  }

  const models = allModels.map((id) => ({
    id,
    object: "model",
    created: new Date().getTime(),
    owned_by: id.includes('claude') ? 'anthropic' : id.includes('gpt') || id.includes('mistral') ? 'mistral' : 'gemini' ||  id.includes('gemini') ? 'openai' : 'ai21' || id.includes('groq') ? 'openai' : 'groq',
    permission: [],
    root: "openai",
    parent: null,
  }));

  modelsCache = { object: "list", data: models };
  modelsCacheTime = new Date().getTime();

  return modelsCache;
};


export { proxyRouter as proxyRouter };
